classdef emptySequence < mcmcBayes.sequence
% emptySequence is a class for demonstrating mcmcBayes with a new model group.  
% * emptyModel - (subtype 'a') prior and posterior data details go here

    properties
        %None
    end
    
    methods (Static)          
        function test
           disp('Hello!'); 
        end
            
        function [uuids]=getUuids()
        % [uuids]=getUuids()
            uuids=[{'emptymodelPriordetailsPosteriordetails'}];   
        end

        function [retSims]=runUnitTests()
        % [retSims]=runUnitTests()
            error('Need to complete this.');%see mcmcBayes.regressionEvaluation
        end
        
        function getData(type, data, varargin)                                
        % getData(obj, type, data, varargin)
        %   This function generates a simulated dataset using the parameter point estimates from varargin.
            error('Need to complete this.');%see mcmcBayes.regressionEvaluation     
        end                             
    end
    
    methods
        function [obj]=emptySequence(sequenceUuid)           
        % [obj]=emptySequence(sequenceUuid)
        %   The constructor for emptySequence.
        %   Input:
        %     sequenceUuid - 
        %   Output:
        %     obj - Constructed emptySequence object
            warning('Need to update this.');

            if nargin > 0
                warning('Replace the mcmcBayes.t_sequence type below');
                superargs{1}=mcmcBayes.t_sequence.emptySequence;
                superargs{2}=sequenceUuid;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});            
        end        
    end
    
    methods (Static, Access=private)     
   
    end    
    
    methods %getter/setter functions

    end
end